#!/usr/bin/env bash

sudo gitlab-runner register --non-interactive \
	--url "https://git.spbpu.com/" \
	--registration-token "${GITLAB_TOKEN}" \
	--executor "${EXECUTOR}" \
	--description "Temp runner on aws instance" \
	--tag-list "${TAGS}" --docker-image "alpine:latest"
