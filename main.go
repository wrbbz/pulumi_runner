package main

import (
	"fmt"
	"github.com/pulumi/pulumi-aws/sdk/v4/go/aws/ec2"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi/config"
	"io/ioutil"
	"strings"
)

func main() {
	pulumi.Run(func(ctx *pulumi.Context) error {
		conf := config.New(ctx, "")
		installationScriptPath := conf.Require("script_path")
		executor, err := conf.Try("executor")
		if err != nil {
			executor = "shell"
		}
		ami, err := conf.Try("ami")
		if err != nil {
			ami = "ami-07d1a833851ff04ce"
		}
		instance, err := conf.Try("instance")
		if err != nil {
			instance = "t4g.medium"
		}
		tag_name := conf.Require("tags")
		gitlab_token := conf.Require("gitlab_token")
		content, err := ioutil.ReadFile(installationScriptPath)
		if err != nil {
			fmt.Println("Error: ", err)
		}

		installationScript := string(content)
		installationScript = strings.Replace(installationScript, "${GITLAB_TOKEN}", gitlab_token, 1)
		installationScript = strings.Replace(installationScript, "${EXECUTOR}", executor, 1)
		installationScript = strings.Replace(installationScript, "${TAGS}", tag_name, 1)
		if executor == "shell" {
			installationScript = strings.Replace(installationScript, "--docker-image \"alpine:latest\"", "", 1)
		}
		fmt.Println(installationScript)

		_, err = ec2.NewInstance(ctx, "arm64_runner", &ec2.InstanceArgs{
			Ami:          pulumi.String(ami),
			InstanceType: pulumi.String(instance),
			UserData:     pulumi.String(installationScript),
			CreditSpecification: &ec2.InstanceCreditSpecificationArgs{
				CpuCredits: pulumi.String("unlimited"),
			},
		})
		if err != nil {
			return err
		}
		return nil
	})
}
