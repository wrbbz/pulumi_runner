#!/usr/bin/env bash
sudo snap install cmake --classic
sudo apt update
sudo apt install --assume-yes software-properties-common gnupg2 \
		libboost-all-dev qt5-default libcurl4-openssl-dev \
		libprotobuf-dev=3.0.0-9.1ubuntu1 build-essential libmysql++-dev \
		libpq-dev libfreetype6-dev libopencv-dev=3.2.0+dfsg-4ubuntu0.1 \
		git protobuf-compiler

sudo apt-key adv --batch --keyserver hkp://keyserver.ubuntu.com --recv-keys AFF709FB200C09FA
sudo apt-add-repository "deb https://${REPO_USER}:${REPO_PASS}@nexus.spbpu.com/repository/ISSDP bionic main"
sudo apt update
sudo apt install -assume-yes spdlog streebog gcryptsigner

curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt install gitlab-runner

sudo gitlab-runner register --non-interactive \
	--url "https://git.spbpu.com/" \
	--registration-token "${GITLAB_TOKEN}" \
	--executor "shell" \
	--description "Temp runner on aws instance" \
	--tag-list "arm64_aws_tmp"
