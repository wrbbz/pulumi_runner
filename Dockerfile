FROM golang:alpine

RUN apk add --update curl && \
	adduser -D pulumi
WORKDIR /home/pulumi
USER pulumi

RUN curl -fsSL https://get.pulumi.com | sh

ENV PATH=${PATH}:/home/pulumi/.pulumi/bin/

COPY --chown=pulumi . runner
